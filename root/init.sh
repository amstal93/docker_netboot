#!/bin/sh

TFTP_DIR=/var/lib/tftpboot

# ---------- GETTING ISO ----------

# extract filename from url
ISO=$( echo $ISO_URL | awk -F "/" '{print $NF}' )
ISO_L="/storage/$ISO"

# if required iso is not present download it.
if [ ! -f "$ISO_L" ]; then
	echo -e "❌ $ISO is not present\n📥 Downloading"
	# TODO - check with SHA SUM
	wget -P /storage/ $ISO_URL
else
	echo "✅ $ISO is present"
fi

# symlink iso to www folder
ln -s $ISO_L /var/www
echo "✅ ISO symlink is present."

# get vmlinuz and initrd from iso
# TODO - use find instead of static path
for file in vmlinuz initrd
do
	7z e $ISO_L casper/$file -o$TFTP_DIR
done
echo "✅ vmlinuz and initrd are present."


# ---------- SYSLINUX FILES ---------- 

# create dir for efi64 files
mkdir /var/lib/tftpboot/efi64

# copy required files to tftp folder
for file in syslinux.efi ldlinux.e64 libutil.c32 libcom32.c32 vesamenu.c32 reboot.c32 
do
	cp /usr/share/syslinux/efi64/$file /var/lib/tftpboot/efi64/
done
echo "✅ syslinux files are present."


# ---------- AUTOINSTALL FILES ----------

# symlink user-data to /var/www
ln -s /storage/user-data /var/www/cloud-init/
echo "✅ user-data symlink is present."


# ---------- CONFIGURING SERVICES ----------

# set ip address
[ -z "$IP_ADDR" ] && IP_ADDR=$(hostname -i)
IP_NET=$(echo "$IP_ADDR" | awk -F. '{print $1"."$2"."$3".0"}')

# update ip addresses
sed -i "s/<ip_net>/$IP_NET/g" /etc/dnsmasq.d/netboot.conf
sed -i "s/<ip_addr>/$IP_ADDR/g" /etc/apache2/httpd.conf
sed -i "s/<ip_addr>/$IP_ADDR/g" /var/lib/tftpboot/pxelinux.cfg/default
sed -i "s/<ISO>/$ISO/g" /var/lib/tftpboot/pxelinux.cfg/default
echo "✅ variables in config files are set."


# ---------- STARTING SERVICES ----------
httpd
dnsmasq 
echo "😈 Services started."
sleep infinte
